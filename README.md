### Wikipedia Android app

This repository contains the source code of the domain-fronted Wikipedia Android app, edited from [apps-android-wikipedia](https://github.com/wikimedia/apps-android-wikipedia).

### Documentation